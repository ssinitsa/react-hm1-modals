import React, { Component } from 'react';
import './App.css';

import Button from './components/Button';
import Modal from './components/Modal';
import ModalButton from './components/ModalButton';

class App extends Component {
  state = {
    isActiveModal1: false,
    isActiveModal2: false,
  }

  toggleModal1 = (e) => {
    if (e.target !== e.currentTarget) {
      return;
    }

    this.setState(state => {
      return {
        isActiveModal1: !this.state.isActiveModal1
      }
    });
  }

  toggleModal2 = (e) => {
    if (e.target !== e.currentTarget) {
      return;
    }

    this.setState(state => {
      return {
        isActiveModal2: !this.state.isActiveModal2
      }
    });
  }

  render() {
    const { isActiveModal1, isActiveModal2 } = this.state;

    const modal1 = <Modal
      closeModal={this.toggleModal1}
      header='Modal window 1'
      text='In by an appetite no humoured returned informed. Possession so comparison inquietude he he conviction no decisively. Marianne jointure attended she hastened surprise but she. Ever lady son yet you very paid form away. He advantage of exquisite resolving if on tolerably. Become sister on in garden it barton waited on.'
      actions={[<ModalButton title="Twitter" />, <ModalButton title="Facebook" />, <ModalButton title="Google" />]}
      closeButton
    />;

    const modal2 = <Modal
      closeModal={this.toggleModal2}
      header='Modal window 2'
      actions={[<ModalButton title="Twitter" />, <ModalButton title="Facebook" />, <ModalButton title="Google" />, <ModalButton title="Amazon" />]}
      text='On am we offices expense thought. Its hence ten smile age means. Seven chief sight far point any. Of so high into easy. Dashwoods eagerness oh extensive as discourse sportsman frankness. Husbands see disposed surprise likewise humoured yet pleasure. Fifteen no inquiry cordial so resolve garrets as. Impression was estimating surrounded solicitude indulgence son shy.'
    />;

    return (
      <div className="App">
        {isActiveModal1 && modal1}
        {isActiveModal2 && modal2}
        <Button backgroundColor='coral' clickHandler={this.toggleModal1} text='First modal' />
        <Button backgroundColor='lightblue' clickHandler={this.toggleModal2} text='Second modal' />
      </div>
    );
  }
}

export default App;
