import React from 'react';

import './Button.scss';

const Button = ({ backgroundColor, text, clickHandler }) => {
  const colorClass = backgroundColor;

  return (
    <button style={{ backgroundColor }} onClick={clickHandler} className="Button">
      {text}
    </button>
  );
}

export default Button;