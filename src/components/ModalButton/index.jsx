import React from 'react';

import './ModalButon.scss';

const ModalButton = ({ title }) => {
  return <button className="ModalButton">{title}</button>
}

export default ModalButton;