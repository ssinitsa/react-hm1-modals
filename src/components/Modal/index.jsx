import React from 'react';

import './Modal.scss';

const Modal = ({ closeModal, header, closeButton, text, actions }) => {
  return (
    <div className="Modal-background" onClick={e => closeModal(e)}>
      <div className="Modal-window">
        {closeButton && <a className="Modal-close" onClick={closeModal}>&#10006;</a>}
        <h2 className="Modal-header">{header}</h2>
        <p className="Modal-text">{text}</p>
        <div className="Modal-buttons">{actions}</div>
      </div>
    </div>
  );
}

export default Modal;